﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Penteen
{
    class GameRender: IDisposable
    {
        private enum Orientation {Up, Down};
        private readonly GameField gameField;
        private readonly int startX, startY;
        private readonly int plateWidth, plateHeight;
        private int selectedPlateSlot;
        private Point[] triangles;
        private Orientation[] orientations;
        private Bitmap[] imageNumbers;
        private Bitmap imageRing;
        private Bitmap imagePlateUp, imagePlateDown;
        private Bitmap imageEmptyUp, imageEmptyDown;
        private Bitmap bitmap;
        private int testX1, testY1;
        private int testX2, testY2;

        public GameRender(GameField gameField,
            int width, int height, int x, int y)
        {
            this.gameField = gameField;
            startX = x;
            startY = y;
            triangles = new Point[GameField.SLOT_COUNT];
            orientations = new Orientation[GameField.SLOT_COUNT];
            imageNumbers = new Bitmap[]
            {
                Properties.Resources.N1, Properties.Resources.N2,
                Properties.Resources.N3, Properties.Resources.N4,
                Properties.Resources.N5, Properties.Resources.N6,
                Properties.Resources.N7, Properties.Resources.N8,
                Properties.Resources.N9, Properties.Resources.N10,
                Properties.Resources.N11, Properties.Resources.N12,
                Properties.Resources.N13, Properties.Resources.N14,
                Properties.Resources.N15
            };
            imageRing = Properties.Resources.Ring;
            imagePlateUp = Properties.Resources.PlateUp;
            imagePlateDown = Properties.Resources.PlateDown;
            imageEmptyUp = Properties.Resources.EmptyUp;
            imageEmptyDown = Properties.Resources.EmptyDown;
            plateWidth = imagePlateUp.Width;
            plateHeight = imagePlateDown.Height;
            selectedPlateSlot = 0;
            CalculateTriangles();
            bitmap = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.Clear(Color.ForestGreen);
            }
            testX1 = 0;
            testY1 = 0;
            testX2 = 0;
            testY2 = 0;
            Repaint();
        }

        private static bool Odd(int x)
        {
            if ((x & 1) == 0)
                return false;
            else
                return true;
        }

        private static int Square(int x)
        {
            return x * x;
        }

        private void CalculateLine(int x, int y, int slot1, int slot2)
        {
            int x1 = x;
            int w = plateWidth;
            for (int i = slot1; i <= slot2; i++)
            {
                orientations[i - 1] = Odd(i - slot1) ?
                    Orientation.Down : Orientation.Up;
                triangles[i - 1].X = x1;
                triangles[i - 1].Y = y;
                x1 += w / 2;
            }
        }

        private void CalculateTriangles()
        {
            int x0 = startX;
            int y0 = startY;
            int w = plateWidth;
            int h = plateHeight;
            // slot 1
            orientations[0] = Orientation.Up;
            triangles[0].X = x0 + w + w / 2;
            triangles[0].Y = y0;
            // triangles 2, 3, 4
            CalculateLine(x0 + w, y0 + h, 2, 4);
            // triangles 5, 6, 7, 8, 9
            CalculateLine(x0 + w / 2, y0 + h + h, 5, 9);
            // triangles 10, 11, 12, 13, 14, 15
            CalculateLine(x0, y0 + h + h + h, 10, 16);
        }

        public void RepaintPlate(Graphics g, int slot)
        {
            int x1 = triangles[slot - 1].X;
            int y1 = triangles[slot - 1].Y;
            int x2 = x1 + 31;
            int x3 = x1 + 24;
            int y2 = y1 + 43;
            int y3 = y1 + 32;
            if (orientations[slot - 1] == Orientation.Down)
            {
                y2 = y1 + 22;
                y3 = y1 + 32 - 24;
            }
            if (gameField.GetPlate(slot) > 0)
            {
                Bitmap face = (orientations[slot - 1] == Orientation.Down) ?
                    imagePlateDown : imagePlateUp;
                Bitmap number = imageNumbers[gameField.GetPlate(slot) - 1];
                g.DrawImageUnscaled(face, x1, y1);
                if (selectedPlateSlot == slot)
                    g.DrawImageUnscaled(imageRing, x3, y3);
                g.DrawImageUnscaled(number, x2, y2);
            }
            else
            {
                Bitmap face = (orientations[slot - 1] == Orientation.Down) ?
                    imageEmptyDown : imageEmptyUp;
                g.DrawImageUnscaled(face, x1, y1);
            }
        }

        public void Repaint()
        {
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                g.Clear(Color.ForestGreen);
                for (int i = 1; i <= GameField.SLOT_COUNT; i++)
                    RepaintPlate(g, i);
                using (Pen whitePen = new Pen(Color.White))
                {
                    //g.DrawLine(whitePen, testX1, testY1, testX2, testY2);
                }
            }
        }

        public void SelectPlate(int slot)
        {
            if (slot == 0)
            {
                selectedPlateSlot = 0;
            }
            else if (slot >= 1 && slot <= GameField.SLOT_COUNT)
            {
                if (gameField.GetPlate(slot) == 0)
                    selectedPlateSlot = 0;
                else
                    selectedPlateSlot = slot;
            }
        }

        private bool IsPlateOnPoint(int slot, int x, int y)
        {
            // get center and radius of internal circle
            int r = plateHeight / 3;
            int xc = triangles[slot - 1].X + plateWidth / 2;
            int yc = (orientations[slot - 1] == Orientation.Down) ?
                triangles[slot - 1].Y + plateHeight - 2 * plateHeight / 3:
                triangles[slot - 1].Y + 2 * plateHeight / 3;
            // check (x, y) in plate triangle
            if (Square(x - xc) + Square(y - yc) < Square(r))
            {
                testX1 = x;
                testY1 = y;
                testX2 = xc;
                testY2 = yc;
                return true;
            }
            else
                return false;
        }

        public int GetPlateOnPoint(int x, int y)
        {
            bool flagInside = false;
            int slot = 1;
            while (slot <= GameField.SLOT_COUNT && !flagInside)
            {
                flagInside = IsPlateOnPoint(slot, x, y);
                if (!flagInside)
                    slot++;
            }
            if (flagInside)
                return slot;
            else
                return 0;
        }

        public Bitmap Bitmap
        {
            get { return bitmap; }
        }

        public void Dispose()
        {
            imagePlateUp.Dispose();
            imagePlateDown.Dispose();
            imageEmptyUp.Dispose();
            imageEmptyDown.Dispose();
            imageRing.Dispose();
            foreach (Bitmap image in imageNumbers)
                image.Dispose();
            bitmap.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
