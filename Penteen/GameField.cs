﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Penteen
{
    class GameField
    {
        public const int SLOT_COUNT = 16;
        private int[] slots;
        private int[] vertNeightbors;
        private int[] leftNeightbors;
        private int[] rightNeightbors;
        //private int emptySlot;
        private Random random;

        public GameField()
        {
            slots = new int[SLOT_COUNT];
            vertNeightbors = new int[SLOT_COUNT];
            leftNeightbors = new int[SLOT_COUNT];
            rightNeightbors = new int[SLOT_COUNT];
            SetupPlates();
            SetupNeightbors();
            random = new Random();
        }

        public void SetupPlates()
        {
            for (int i = 1; i < SLOT_COUNT; i++)
                slots[i - 1] = i;
            slots[SLOT_COUNT - 1] = 0;
            //emptySlot = SLOT_COUNT;
        }

        private void SetupSlotNeightbors(int slot, int vert, int left, int right)
        {
            vertNeightbors[slot - 1] = vert;
            leftNeightbors[slot - 1] = left;
            rightNeightbors[slot - 1] = right;
        }

        private void SetupNeightbors()
        {
            // slot 1
            SetupSlotNeightbors(1, 3, 10, 16);
            // slots 2, 3, 4
            SetupSlotNeightbors(2, 6, 0, 3);
            SetupSlotNeightbors(3, 1, 2, 4);
            SetupSlotNeightbors(4, 8, 3, 0);
            // slots 5, 6, 7, 8, 9
            SetupSlotNeightbors(5, 11, 0, 6);
            SetupSlotNeightbors(6, 2, 5, 7);
            SetupSlotNeightbors(7, 13, 6, 8);
            SetupSlotNeightbors(8, 4, 7, 9);
            SetupSlotNeightbors(9, 15, 8, 0);
            // slots 10, 11, 12, 13, 14, 15, 16
            SetupSlotNeightbors(10, 1, 16, 11);
            SetupSlotNeightbors(11, 5, 10, 12);
            SetupSlotNeightbors(12, 0, 11, 13);
            SetupSlotNeightbors(13, 7, 12, 14);
            SetupSlotNeightbors(14, 0, 13, 15);
            SetupSlotNeightbors(15, 9, 14, 16);
            SetupSlotNeightbors(16, 1, 15, 10);
        }

        private int SelectRandomNeightbor(int slot)
        {
            int neightbor = 0;
            int k = random.Next(3);
            switch (k)
            {
                case 0:
                    neightbor = vertNeightbors[slot - 1];
                    break;
                case 1:
                    neightbor = leftNeightbors[slot - 1];
                    break;
                case 2:
                    neightbor = rightNeightbors[slot - 1];
                    break;
                default:
                    throw new Exception("SelectRandomNeightbor: invalid random value");
            }
            return neightbor;
        }

        public void SwapPlates(int slot1, int slot2)
        {
            if (slot1 < 1 || slot1 > SLOT_COUNT)
                return;
            if (slot2 < 1 || slot2 > SLOT_COUNT)
                return;
            int temp = slots[slot1 - 1];
            slots[slot1 - 1] = slots[slot2 - 1];
            slots[slot2 - 1] = temp;
        }

        public void MixPlates(int mixCount)
        {
            int p0 = 0;
            int p = 16;
            for (int i = 0; i < mixCount; i++)
            {
                // select random neightbor
                int n = 0;
                while (n == 0 || n == p0)
                {
                    n = SelectRandomNeightbor(p);
                }
                // swap plates
                SwapPlates(p, n);
                // no back move
                p0 = p;
                // plate n is empty
                p = n;
            }
        }

        public int GetPlate(int slot)
        {
            if (slot >= 1 && slot <= SLOT_COUNT)
                return slots[slot - 1];
            else
                return 0;
        }

        public int GetVerticalNeightbor(int slot)
        {
            if (slot >= 1 && slot <= SLOT_COUNT)
                return vertNeightbors[slot - 1];
            else
                return 0;
        }

        public int GetLeftNeightbor(int slot)
        {
            if (slot >= 1 && slot <= SLOT_COUNT)
                return leftNeightbors[slot - 1];
            else
                return 0;
        }

        public int GetRightNeightbor(int slot)
        {
            if (slot >= 1 && slot <= SLOT_COUNT)
                return rightNeightbors[slot - 1];
            else
                return 0;
        }

        public bool IsCompletePlates()
        {
            int slot = 1;
            bool flagComplete = true;
            while (slot < 16 && flagComplete)
            {
                if (slots[slot - 1] != slot)
                    flagComplete = false;
                slot++;
            }
            if (slots[16 - 1] != 0)
                flagComplete = false;
            return flagComplete;
        }
    }
}
