﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.IO;

namespace Penteen
{
    partial class RecordsForm : Form
    {
        private const int RECORD_COUNT = 10;
        private RecordInfo recordInfo;
        private string recordFileName;

        public RecordsForm()
        {
            InitializeComponent();
            recordFileName = "";
            recordInfo = new RecordInfo(RECORD_COUNT, "Anonymouse", 7199);
        }

        public RecordInfo RecordInfo
        {
            get { return recordInfo; }
        }

        public string RecordFile
        {
            set { recordFileName = value; }
            get { return recordFileName; }
        }

        private void ShowRecords()
        {
            Label[] labelNames = new Label[]
            {
                labelName1, labelName2, labelName3, labelName4, labelName5,
                labelName6, labelName7, labelName8, labelName9, labelName10
            };
            Label[] labelTimes = new Label[]
            {
                labelTime1, labelTime2, labelTime3, labelTime4, labelTime5,
                labelTime6, labelTime7, labelTime8, labelTime9, labelTime10
            };
            for (int i = 0; i < RECORD_COUNT; i++)
            {
                labelNames[i].Text = recordInfo.GetName(i + 1);
                labelTimes[i].Text = RecordInfo.TimeToString(recordInfo.GetTime(i + 1));
            }
        }

        private void RecordsForm_Load(object sender, EventArgs e)
        {
            if (File.Exists(recordFileName))
                recordInfo.LoadRecords(recordFileName);
            ShowRecords();
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            DialogResult r = MessageBox.Show("Стереть рекорды?",
                Application.ProductName, MessageBoxButtons.YesNo);
            okButton.Focus();
            if (r != DialogResult.Yes)
                return;
            recordInfo.ClearRecords();
            if (File.Exists(recordFileName))
                recordInfo.SaveRecords(recordFileName);
            ShowRecords();
        }
    }
}
