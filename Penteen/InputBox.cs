﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace Penteen
{
    partial class InputBox : Form
    {
        public InputBox()
        {
            InitializeComponent();
        }

        new public static string Show()
        {
            InputBox inputBox = new InputBox();
            DialogResult r = inputBox.ShowDialog();
            if (r == DialogResult.OK)
                return inputBox.textLine.Text;
            else
                return "";
        }

        public static string Show(string prompt)
        {
            InputBox inputBox = new InputBox();
            inputBox.labelInput.Text = prompt;
            DialogResult r = inputBox.ShowDialog();
            if (r == DialogResult.OK)
                return inputBox.textLine.Text;
            else
                return "";
        }

        public static string Show(string prompt, string caption)
        {
            InputBox inputBox = new InputBox();
            inputBox.Text = caption;
            inputBox.labelInput.Text = prompt;
            DialogResult r = inputBox.ShowDialog();
            if (r == DialogResult.OK)
                return inputBox.textLine.Text;
            else
                return "";
        }
    }
}
