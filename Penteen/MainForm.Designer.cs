﻿namespace Penteen
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pictureBoxField = new System.Windows.Forms.PictureBox();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.menuItemGame = new System.Windows.Forms.MenuItem();
            this.menuItemGameNew = new System.Windows.Forms.MenuItem();
            this.menuItemGame1 = new System.Windows.Forms.MenuItem();
            this.menuItemGameRecords = new System.Windows.Forms.MenuItem();
            this.menuItemGame2 = new System.Windows.Forms.MenuItem();
            this.menuItemGameExit = new System.Windows.Forms.MenuItem();
            this.menuItemHelp = new System.Windows.Forms.MenuItem();
            this.menuItemHelpContent = new System.Windows.Forms.MenuItem();
            this.menuItemHelp1 = new System.Windows.Forms.MenuItem();
            this.menuItemHelpAbout = new System.Windows.Forms.MenuItem();
            this.secondaryTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxField)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxField
            // 
            this.pictureBoxField.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxField.Name = "pictureBoxField";
            this.pictureBoxField.Size = new System.Drawing.Size(100, 50);
            this.pictureBoxField.TabIndex = 1;
            this.pictureBoxField.TabStop = false;
            this.pictureBoxField.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBoxField_MouseClick);
            this.pictureBoxField.MouseLeave += new System.EventHandler(this.pictureBoxField_MouseLeave);
            this.pictureBoxField.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBoxField_MouseMove);
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemGame,
            this.menuItemHelp});
            // 
            // menuItemGame
            // 
            this.menuItemGame.Index = 0;
            this.menuItemGame.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemGameNew,
            this.menuItemGame1,
            this.menuItemGameRecords,
            this.menuItemGame2,
            this.menuItemGameExit});
            this.menuItemGame.Text = "&Игра";
            // 
            // menuItemGameNew
            // 
            this.menuItemGameNew.Index = 0;
            this.menuItemGameNew.Shortcut = System.Windows.Forms.Shortcut.F2;
            this.menuItemGameNew.Text = "&Новая игра";
            this.menuItemGameNew.Click += new System.EventHandler(this.menuItemGameNew_Click);
            // 
            // menuItemGame1
            // 
            this.menuItemGame1.Index = 1;
            this.menuItemGame1.Text = "-";
            // 
            // menuItemGameRecords
            // 
            this.menuItemGameRecords.Index = 2;
            this.menuItemGameRecords.Shortcut = System.Windows.Forms.Shortcut.F4;
            this.menuItemGameRecords.Text = "&Рекорды...";
            this.menuItemGameRecords.Click += new System.EventHandler(this.menuItemGameRecords_Click);
            // 
            // menuItemGame2
            // 
            this.menuItemGame2.Index = 3;
            this.menuItemGame2.Text = "-";
            // 
            // menuItemGameExit
            // 
            this.menuItemGameExit.Index = 4;
            this.menuItemGameExit.Text = "В&ыход";
            this.menuItemGameExit.Click += new System.EventHandler(this.menuItemGameExit_Click);
            // 
            // menuItemHelp
            // 
            this.menuItemHelp.Index = 1;
            this.menuItemHelp.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemHelpContent,
            this.menuItemHelp1,
            this.menuItemHelpAbout});
            this.menuItemHelp.Text = "&Справка";
            // 
            // menuItemHelpContent
            // 
            this.menuItemHelpContent.Index = 0;
            this.menuItemHelpContent.Shortcut = System.Windows.Forms.Shortcut.F1;
            this.menuItemHelpContent.Text = "&Содержание";
            this.menuItemHelpContent.Click += new System.EventHandler(this.menuItemHelpContent_Click);
            // 
            // menuItemHelp1
            // 
            this.menuItemHelp1.Index = 1;
            this.menuItemHelp1.Text = "-";
            // 
            // menuItemHelpAbout
            // 
            this.menuItemHelpAbout.Index = 2;
            this.menuItemHelpAbout.Text = "&О программе";
            this.menuItemHelpAbout.Click += new System.EventHandler(this.menuItemHelpAbout_Click);
            // 
            // secondaryTimer
            // 
            this.secondaryTimer.Interval = 1000;
            this.secondaryTimer.Tick += new System.EventHandler(this.secondaryTimer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.pictureBoxField);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Menu = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxField)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxField;
        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.MenuItem menuItemGame;
        private System.Windows.Forms.MenuItem menuItemGameExit;
        private System.Windows.Forms.MenuItem menuItemHelp;
        private System.Windows.Forms.MenuItem menuItemHelpAbout;
        private System.Windows.Forms.MenuItem menuItemGameNew;
        private System.Windows.Forms.MenuItem menuItemGame1;
        private System.Windows.Forms.MenuItem menuItemGameRecords;
        private System.Windows.Forms.MenuItem menuItemGame2;
        private System.Windows.Forms.MenuItem menuItemHelpContent;
        private System.Windows.Forms.MenuItem menuItemHelp1;
        private System.Windows.Forms.Timer secondaryTimer;
    }
}

