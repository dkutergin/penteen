﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Penteen
{
    class RecordInfo
    {
        private readonly int count;
        private readonly string defaultName;
        private readonly int defaultTime;
        private string[] names;
        private int[] times;

        public RecordInfo(int count, string defaultName, int defaultTime)
        {
            this.count = count;
            this.defaultName = defaultName;
            this.defaultTime = defaultTime;
            names = new string[count];
            times = new int[count];
            ClearRecords();
        }

        public RecordInfo(int count, string filName)
        {
            this.count = count;
        }

        public int Count
        {
            get { return count; }
        }

        public string DefaultName
        {
            get { return defaultName; }
        }

        public int DefaultTime
        {
            get { return defaultTime; }
        }

        public static string TimeToString(int seconds)
        {
            int sec = seconds % 60;
            int min = (seconds / 60) % 60;
            int hours = seconds / 3600;
            string result = String.Format("{0} сек", sec);
            if (min > 0)
                result = String.Format("{0} мин ", min) + result;
            if (hours > 0)
                result = String.Format("{0} ч ", hours) + result;
            return result;
        }

        public string GetName(int index)
        {
            if (index >= 1 && index <= count - 1)
                return names[index - 1];
            else
                return defaultName;
        }

        public int GetTime(int index)
        {
            if (index >= 1 && index <= count - 1)
                return times[index - 1];
            else
                return defaultTime;
        }

        public string GetTimeAsString(int index)
        {
            return TimeToString(GetTime(index));
        }

        public void ClearRecords()
        {
            for (int i = 0; i < count; i++)
            {
                names[i] = defaultName;
                times[i] = defaultTime;
            }
        }

        public void AddRecord(string name, int time)
        {
            bool found = false;
            int i = 0;
            while (i < count && !found)
            {
                if (times[i] > time)
                    found = true;
                else
                    i++;
            }
            if (!found)
                return;
            names[i] = name;
            times[i] = time;
        }

        public int MinimalTime
        {
            get
            {
                if (count < 1)
                    return int.MaxValue;
                int min = times[0];
                for (int i = 1; i < count; i++)
                {
                    if (times[i] < min)
                        min = times[i];
                }
                return min;
            }
        }

        public void LoadRecords(string fileName)
        {
            XDocument doc = XDocument.Load(fileName);
            if (doc.Root.Name != "Records")
                return;
            int i = 0;
            foreach (XElement node in doc.Root.Elements())
            {
                if (node.Name == "Record" && i < count)
                {
                    names[i] = node.Attribute("Name").Value;
                    times[i] = int.Parse(node.Attribute("Time").Value);
                    i++;
                }
            }
        }

        public void SaveRecords(string fileName)
        {
            XDocument doc = new XDocument();
            XElement rootNode = new XElement("Records");
            doc.Add(rootNode);
            for (int i = 0; i < count; i++)
            {
                XElement recordNode = new XElement("Record");
                recordNode.Add(new XAttribute("Name", names[i]));
                recordNode.Add(new XAttribute("Time", times[i]));
                rootNode.Add(recordNode);
            }
            doc.Save(fileName);
        }
    }
}
