﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Media;

namespace Penteen
{
    public partial class MainForm : Form
    {
        private const int MAX_PLATES_PER_LINE = 4;
        private const int PLATE_LINES = 4;
        private const int PLATE_WIDTH = 95;
        private const int PLATE_HEIGHT = 85;
        private const int FIELD_BORDER_X = 10;
        private const int FIELD_BORDER_Y = 10;
        private GameField field;
        private GameRender render;
        private int playedTime;
        private bool startedNewGame;

        public MainForm()
        {
            InitializeComponent();
            //Icon = Application.
            Text = Application.ProductName;
            int dw = Width - ClientSize.Width;
            int dh = Height - ClientSize.Height;
            Width = dw + MAX_PLATES_PER_LINE * PLATE_WIDTH + 2 * FIELD_BORDER_X;
            Height = dh + PLATE_LINES * PLATE_HEIGHT + 2 * FIELD_BORDER_Y;
            pictureBoxField.Width = ClientSize.Width;
            pictureBoxField.Height = ClientSize.Height;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            field = new GameField();
            render = new GameRender(field,
                pictureBoxField.Width, pictureBoxField.Height,
                FIELD_BORDER_X, FIELD_BORDER_Y);
            pictureBoxField.Image = render.Bitmap;
            startedNewGame = false;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Visible = false;
            render.Dispose();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing || !startedNewGame)
                return;
            secondaryTimer.Enabled = false;
            DialogResult r = MessageBox.Show("Закончить игру?",
                Application.ProductName, MessageBoxButtons.YesNo);
            if (r != DialogResult.Yes)
            {
                secondaryTimer.Enabled = true;
                e.Cancel = true;
            }
        }

        private void pictureBoxField_MouseClick(object sender, MouseEventArgs e)
        {
            if (!startedNewGame)
                return;
            int x = e.X;
            int y = e.Y;
            int slot = render.GetPlateOnPoint(x, y);
            if (field.GetPlate(slot) == 0)
                return;
            int n = field.GetLeftNeightbor(slot);
            if (n == 0 || field.GetPlate(n) != 0)
                n = field.GetRightNeightbor(slot);
            if (n == 0 || field.GetPlate(n) != 0)
                n = field.GetVerticalNeightbor(slot);
            if (n == 0 || field.GetPlate(n) != 0)
            {
                SystemSounds.Exclamation.Play();
                return;
            }
            field.SwapPlates(slot, n);
            render.Repaint();
            pictureBoxField.Invalidate();
            if (field.IsCompletePlates())
            {
                secondaryTimer.Enabled = false;
                Text = Application.ProductName;
                startedNewGame = false;
                SystemSounds.Asterisk.Play();
                MessageBox.Show("Вы выиграли!", Application.ProductName);
                RecordsForm recordsForm = new RecordsForm();
                string recordFile = Application.StartupPath;
                if (recordFile[recordFile.Length - 1] != Path.DirectorySeparatorChar)
                    recordFile += Path.DirectorySeparatorChar;
                recordFile += "Records.xml";
                recordsForm.RecordFile = recordFile;
                RecordInfo recordInfo = recordsForm.RecordInfo;
                if (playedTime < recordInfo.MinimalTime)
                {
                    string username = InputBox.Show("Введите свое имя:",
                        Application.ProductName);
                    if (username == "")
                        username = recordInfo.DefaultName;
                    recordInfo.AddRecord(username, playedTime);
                    recordInfo.SaveRecords(recordFile);
                }
                recordsForm.ShowDialog();
            }
        }

        private void pictureBoxField_MouseMove(object sender, MouseEventArgs e)
        {
            if (!startedNewGame)
                return;
            int x = e.X;
            int y = e.Y;
            int slot = render.GetPlateOnPoint(x, y);
            render.SelectPlate(slot);
            render.Repaint();
            pictureBoxField.Invalidate();
        }

        private void pictureBoxField_MouseLeave(object sender, EventArgs e)
        {
            if (!startedNewGame)
                return;
            render.SelectPlate(0);
            render.Repaint();
            pictureBoxField.Invalidate();
        }

        private void menuItemGameExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void menuItemGameNew_Click(object sender, EventArgs e)
        {
            if (startedNewGame)
            {
                secondaryTimer.Enabled = false;
                DialogResult r = MessageBox.Show("Начать новую игру?",
                    Application.ProductName, MessageBoxButtons.YesNo);
                if (r != DialogResult.Yes)
                {
                    secondaryTimer.Enabled = true;
                    return;
                }
            }
            field.MixPlates(100);
            render.Repaint();
            pictureBoxField.Invalidate();
            playedTime = 0;
            secondaryTimer.Enabled = true;
            startedNewGame = true;
        }

        private void secondaryTimer_Tick(object sender, EventArgs e)
        {
            playedTime++;
            Text = Application.ProductName + " - Время: " +
                RecordInfo.TimeToString(playedTime);
        }

        private void menuItemGameRecords_Click(object sender, EventArgs e)
        {
            if (startedNewGame)
                secondaryTimer.Enabled = false;
            RecordsForm recordForm = new RecordsForm();
            string recordFile = Application.StartupPath;
            if (recordFile[recordFile.Length - 1] != Path.DirectorySeparatorChar)
                recordFile += Path.DirectorySeparatorChar;
            recordFile += "Records.xml";
            recordForm.RecordFile = recordFile;
            recordForm.ShowDialog();
            if (startedNewGame)
                secondaryTimer.Enabled = true;
        }

        private void menuItemHelpContent_Click(object sender, EventArgs e)
        {
            if (startedNewGame)
                secondaryTimer.Enabled = false;
            (new HelpBox()).ShowDialog();
            if (startedNewGame)
                secondaryTimer.Enabled = true;
        }

        private void menuItemHelpAbout_Click(object sender, EventArgs e)
        {
            if (startedNewGame)
                secondaryTimer.Enabled = false;
            (new AboutBox()).ShowDialog();
            if (startedNewGame)
                secondaryTimer.Enabled = true;
        }
    }
}
