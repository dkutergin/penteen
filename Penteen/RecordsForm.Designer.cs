﻿namespace Penteen
{
    partial class RecordsForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.labelName10 = new System.Windows.Forms.Label();
            this.labelName9 = new System.Windows.Forms.Label();
            this.labelName8 = new System.Windows.Forms.Label();
            this.labelName7 = new System.Windows.Forms.Label();
            this.labelName6 = new System.Windows.Forms.Label();
            this.labelName5 = new System.Windows.Forms.Label();
            this.labelName4 = new System.Windows.Forms.Label();
            this.labelName3 = new System.Windows.Forms.Label();
            this.labelName2 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.labelNumber1 = new System.Windows.Forms.Label();
            this.labelNumber2 = new System.Windows.Forms.Label();
            this.labelNumber3 = new System.Windows.Forms.Label();
            this.labelNumber4 = new System.Windows.Forms.Label();
            this.labelNumber5 = new System.Windows.Forms.Label();
            this.labelNumber6 = new System.Windows.Forms.Label();
            this.labelNumber7 = new System.Windows.Forms.Label();
            this.labelNumber8 = new System.Windows.Forms.Label();
            this.labelNumber9 = new System.Windows.Forms.Label();
            this.labelNumber10 = new System.Windows.Forms.Label();
            this.labelName1 = new System.Windows.Forms.Label();
            this.labelTime1 = new System.Windows.Forms.Label();
            this.labelTime2 = new System.Windows.Forms.Label();
            this.labelTime3 = new System.Windows.Forms.Label();
            this.labelTime4 = new System.Windows.Forms.Label();
            this.labelTime5 = new System.Windows.Forms.Label();
            this.labelTime6 = new System.Windows.Forms.Label();
            this.labelTime7 = new System.Windows.Forms.Label();
            this.labelTime8 = new System.Windows.Forms.Label();
            this.labelTime9 = new System.Windows.Forms.Label();
            this.labelTime10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 3;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.42857F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.39682F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.1746F));
            this.tableLayoutPanel.Controls.Add(this.labelName10, 1, 9);
            this.tableLayoutPanel.Controls.Add(this.labelName9, 1, 8);
            this.tableLayoutPanel.Controls.Add(this.labelName8, 1, 7);
            this.tableLayoutPanel.Controls.Add(this.labelName7, 1, 6);
            this.tableLayoutPanel.Controls.Add(this.labelName6, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.labelName5, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.labelName4, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.labelName3, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.labelName2, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.okButton, 1, 10);
            this.tableLayoutPanel.Controls.Add(this.clearButton, 2, 10);
            this.tableLayoutPanel.Controls.Add(this.labelNumber1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.labelNumber2, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.labelNumber3, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.labelNumber4, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.labelNumber5, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.labelNumber6, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.labelNumber7, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.labelNumber8, 0, 7);
            this.tableLayoutPanel.Controls.Add(this.labelNumber9, 0, 8);
            this.tableLayoutPanel.Controls.Add(this.labelNumber10, 0, 9);
            this.tableLayoutPanel.Controls.Add(this.labelName1, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.labelTime1, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.labelTime2, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.labelTime3, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.labelTime4, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.labelTime5, 2, 4);
            this.tableLayoutPanel.Controls.Add(this.labelTime6, 2, 5);
            this.tableLayoutPanel.Controls.Add(this.labelTime7, 2, 6);
            this.tableLayoutPanel.Controls.Add(this.labelTime8, 2, 7);
            this.tableLayoutPanel.Controls.Add(this.labelTime9, 2, 8);
            this.tableLayoutPanel.Controls.Add(this.labelTime10, 2, 9);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(9, 9);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 11;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(315, 301);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // labelName10
            // 
            this.labelName10.AutoSize = true;
            this.labelName10.Location = new System.Drawing.Point(39, 243);
            this.labelName10.Name = "labelName10";
            this.labelName10.Size = new System.Drawing.Size(68, 13);
            this.labelName10.TabIndex = 45;
            this.labelName10.Text = "Anonymouse";
            // 
            // labelName9
            // 
            this.labelName9.AutoSize = true;
            this.labelName9.Location = new System.Drawing.Point(39, 216);
            this.labelName9.Name = "labelName9";
            this.labelName9.Size = new System.Drawing.Size(68, 13);
            this.labelName9.TabIndex = 44;
            this.labelName9.Text = "Anonymouse";
            // 
            // labelName8
            // 
            this.labelName8.AutoSize = true;
            this.labelName8.Location = new System.Drawing.Point(39, 189);
            this.labelName8.Name = "labelName8";
            this.labelName8.Size = new System.Drawing.Size(68, 13);
            this.labelName8.TabIndex = 43;
            this.labelName8.Text = "Anonymouse";
            // 
            // labelName7
            // 
            this.labelName7.AutoSize = true;
            this.labelName7.Location = new System.Drawing.Point(39, 162);
            this.labelName7.Name = "labelName7";
            this.labelName7.Size = new System.Drawing.Size(68, 13);
            this.labelName7.TabIndex = 42;
            this.labelName7.Text = "Anonymouse";
            // 
            // labelName6
            // 
            this.labelName6.AutoSize = true;
            this.labelName6.Location = new System.Drawing.Point(39, 135);
            this.labelName6.Name = "labelName6";
            this.labelName6.Size = new System.Drawing.Size(68, 13);
            this.labelName6.TabIndex = 41;
            this.labelName6.Text = "Anonymouse";
            // 
            // labelName5
            // 
            this.labelName5.AutoSize = true;
            this.labelName5.Location = new System.Drawing.Point(39, 108);
            this.labelName5.Name = "labelName5";
            this.labelName5.Size = new System.Drawing.Size(68, 13);
            this.labelName5.TabIndex = 40;
            this.labelName5.Text = "Anonymouse";
            // 
            // labelName4
            // 
            this.labelName4.AutoSize = true;
            this.labelName4.Location = new System.Drawing.Point(39, 81);
            this.labelName4.Name = "labelName4";
            this.labelName4.Size = new System.Drawing.Size(68, 13);
            this.labelName4.TabIndex = 39;
            this.labelName4.Text = "Anonymouse";
            // 
            // labelName3
            // 
            this.labelName3.AutoSize = true;
            this.labelName3.Location = new System.Drawing.Point(39, 54);
            this.labelName3.Name = "labelName3";
            this.labelName3.Size = new System.Drawing.Size(68, 13);
            this.labelName3.TabIndex = 38;
            this.labelName3.Text = "Anonymouse";
            // 
            // labelName2
            // 
            this.labelName2.AutoSize = true;
            this.labelName2.Location = new System.Drawing.Point(39, 27);
            this.labelName2.Name = "labelName2";
            this.labelName2.Size = new System.Drawing.Size(68, 13);
            this.labelName2.TabIndex = 37;
            this.labelName2.Text = "Anonymouse";
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(70, 275);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 24;
            this.okButton.Text = "&ОК";
            // 
            // clearButton
            // 
            this.clearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clearButton.Location = new System.Drawing.Point(182, 275);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 25;
            this.clearButton.Text = "&Clear";
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // labelNumber1
            // 
            this.labelNumber1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber1.AutoSize = true;
            this.labelNumber1.Location = new System.Drawing.Point(20, 0);
            this.labelNumber1.Name = "labelNumber1";
            this.labelNumber1.Size = new System.Drawing.Size(13, 13);
            this.labelNumber1.TabIndex = 26;
            this.labelNumber1.Text = "1";
            // 
            // labelNumber2
            // 
            this.labelNumber2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber2.AutoSize = true;
            this.labelNumber2.Location = new System.Drawing.Point(20, 27);
            this.labelNumber2.Name = "labelNumber2";
            this.labelNumber2.Size = new System.Drawing.Size(13, 13);
            this.labelNumber2.TabIndex = 27;
            this.labelNumber2.Text = "2";
            // 
            // labelNumber3
            // 
            this.labelNumber3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber3.AutoSize = true;
            this.labelNumber3.Location = new System.Drawing.Point(20, 54);
            this.labelNumber3.Name = "labelNumber3";
            this.labelNumber3.Size = new System.Drawing.Size(13, 13);
            this.labelNumber3.TabIndex = 28;
            this.labelNumber3.Text = "3";
            // 
            // labelNumber4
            // 
            this.labelNumber4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber4.AutoSize = true;
            this.labelNumber4.Location = new System.Drawing.Point(20, 81);
            this.labelNumber4.Name = "labelNumber4";
            this.labelNumber4.Size = new System.Drawing.Size(13, 13);
            this.labelNumber4.TabIndex = 29;
            this.labelNumber4.Text = "4";
            // 
            // labelNumber5
            // 
            this.labelNumber5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber5.AutoSize = true;
            this.labelNumber5.Location = new System.Drawing.Point(20, 108);
            this.labelNumber5.Name = "labelNumber5";
            this.labelNumber5.Size = new System.Drawing.Size(13, 13);
            this.labelNumber5.TabIndex = 30;
            this.labelNumber5.Text = "5";
            // 
            // labelNumber6
            // 
            this.labelNumber6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber6.AutoSize = true;
            this.labelNumber6.Location = new System.Drawing.Point(20, 135);
            this.labelNumber6.Name = "labelNumber6";
            this.labelNumber6.Size = new System.Drawing.Size(13, 13);
            this.labelNumber6.TabIndex = 31;
            this.labelNumber6.Text = "6";
            // 
            // labelNumber7
            // 
            this.labelNumber7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber7.AutoSize = true;
            this.labelNumber7.Location = new System.Drawing.Point(20, 162);
            this.labelNumber7.Name = "labelNumber7";
            this.labelNumber7.Size = new System.Drawing.Size(13, 13);
            this.labelNumber7.TabIndex = 32;
            this.labelNumber7.Text = "7";
            // 
            // labelNumber8
            // 
            this.labelNumber8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber8.AutoSize = true;
            this.labelNumber8.Location = new System.Drawing.Point(20, 189);
            this.labelNumber8.Name = "labelNumber8";
            this.labelNumber8.Size = new System.Drawing.Size(13, 13);
            this.labelNumber8.TabIndex = 33;
            this.labelNumber8.Text = "8";
            // 
            // labelNumber9
            // 
            this.labelNumber9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber9.AutoSize = true;
            this.labelNumber9.Location = new System.Drawing.Point(20, 216);
            this.labelNumber9.Name = "labelNumber9";
            this.labelNumber9.Size = new System.Drawing.Size(13, 13);
            this.labelNumber9.TabIndex = 34;
            this.labelNumber9.Text = "9";
            // 
            // labelNumber10
            // 
            this.labelNumber10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNumber10.AutoSize = true;
            this.labelNumber10.Location = new System.Drawing.Point(14, 243);
            this.labelNumber10.Name = "labelNumber10";
            this.labelNumber10.Size = new System.Drawing.Size(19, 13);
            this.labelNumber10.TabIndex = 35;
            this.labelNumber10.Text = "10";
            // 
            // labelName1
            // 
            this.labelName1.AutoSize = true;
            this.labelName1.Location = new System.Drawing.Point(39, 0);
            this.labelName1.Name = "labelName1";
            this.labelName1.Size = new System.Drawing.Size(68, 13);
            this.labelName1.TabIndex = 36;
            this.labelName1.Text = "Anonymouse";
            // 
            // labelTime1
            // 
            this.labelTime1.AutoSize = true;
            this.labelTime1.Location = new System.Drawing.Point(182, 0);
            this.labelTime1.Name = "labelTime1";
            this.labelTime1.Size = new System.Drawing.Size(33, 13);
            this.labelTime1.TabIndex = 46;
            this.labelTime1.Text = "0 sec";
            // 
            // labelTime2
            // 
            this.labelTime2.AutoSize = true;
            this.labelTime2.Location = new System.Drawing.Point(182, 27);
            this.labelTime2.Name = "labelTime2";
            this.labelTime2.Size = new System.Drawing.Size(33, 13);
            this.labelTime2.TabIndex = 47;
            this.labelTime2.Text = "0 sec";
            // 
            // labelTime3
            // 
            this.labelTime3.AutoSize = true;
            this.labelTime3.Location = new System.Drawing.Point(182, 54);
            this.labelTime3.Name = "labelTime3";
            this.labelTime3.Size = new System.Drawing.Size(33, 13);
            this.labelTime3.TabIndex = 48;
            this.labelTime3.Text = "0 sec";
            // 
            // labelTime4
            // 
            this.labelTime4.AutoSize = true;
            this.labelTime4.Location = new System.Drawing.Point(182, 81);
            this.labelTime4.Name = "labelTime4";
            this.labelTime4.Size = new System.Drawing.Size(33, 13);
            this.labelTime4.TabIndex = 49;
            this.labelTime4.Text = "0 sec";
            // 
            // labelTime5
            // 
            this.labelTime5.AutoSize = true;
            this.labelTime5.Location = new System.Drawing.Point(182, 108);
            this.labelTime5.Name = "labelTime5";
            this.labelTime5.Size = new System.Drawing.Size(33, 13);
            this.labelTime5.TabIndex = 50;
            this.labelTime5.Text = "0 sec";
            // 
            // labelTime6
            // 
            this.labelTime6.AutoSize = true;
            this.labelTime6.Location = new System.Drawing.Point(182, 135);
            this.labelTime6.Name = "labelTime6";
            this.labelTime6.Size = new System.Drawing.Size(33, 13);
            this.labelTime6.TabIndex = 51;
            this.labelTime6.Text = "0 sec";
            // 
            // labelTime7
            // 
            this.labelTime7.AutoSize = true;
            this.labelTime7.Location = new System.Drawing.Point(182, 162);
            this.labelTime7.Name = "labelTime7";
            this.labelTime7.Size = new System.Drawing.Size(33, 13);
            this.labelTime7.TabIndex = 52;
            this.labelTime7.Text = "0 sec";
            // 
            // labelTime8
            // 
            this.labelTime8.AutoSize = true;
            this.labelTime8.Location = new System.Drawing.Point(182, 189);
            this.labelTime8.Name = "labelTime8";
            this.labelTime8.Size = new System.Drawing.Size(33, 13);
            this.labelTime8.TabIndex = 53;
            this.labelTime8.Text = "0 sec";
            // 
            // labelTime9
            // 
            this.labelTime9.AutoSize = true;
            this.labelTime9.Location = new System.Drawing.Point(182, 216);
            this.labelTime9.Name = "labelTime9";
            this.labelTime9.Size = new System.Drawing.Size(33, 13);
            this.labelTime9.TabIndex = 54;
            this.labelTime9.Text = "0 sec";
            // 
            // labelTime10
            // 
            this.labelTime10.AutoSize = true;
            this.labelTime10.Location = new System.Drawing.Point(182, 243);
            this.labelTime10.Name = "labelTime10";
            this.labelTime10.Size = new System.Drawing.Size(33, 13);
            this.labelTime10.TabIndex = 55;
            this.labelTime10.Text = "0 sec";
            // 
            // RecordsForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 319);
            this.Controls.Add(this.tableLayoutPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RecordsForm";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Рекорды";
            this.Load += new System.EventHandler(this.RecordsForm_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Label labelName10;
        private System.Windows.Forms.Label labelName9;
        private System.Windows.Forms.Label labelName8;
        private System.Windows.Forms.Label labelName7;
        private System.Windows.Forms.Label labelName6;
        private System.Windows.Forms.Label labelName5;
        private System.Windows.Forms.Label labelName4;
        private System.Windows.Forms.Label labelName3;
        private System.Windows.Forms.Label labelName2;
        private System.Windows.Forms.Label labelNumber1;
        private System.Windows.Forms.Label labelNumber2;
        private System.Windows.Forms.Label labelNumber3;
        private System.Windows.Forms.Label labelNumber4;
        private System.Windows.Forms.Label labelNumber5;
        private System.Windows.Forms.Label labelNumber6;
        private System.Windows.Forms.Label labelNumber7;
        private System.Windows.Forms.Label labelNumber8;
        private System.Windows.Forms.Label labelNumber9;
        private System.Windows.Forms.Label labelNumber10;
        private System.Windows.Forms.Label labelName1;
        private System.Windows.Forms.Label labelTime10;
        private System.Windows.Forms.Label labelTime9;
        private System.Windows.Forms.Label labelTime8;
        private System.Windows.Forms.Label labelTime7;
        private System.Windows.Forms.Label labelTime6;
        private System.Windows.Forms.Label labelTime5;
        private System.Windows.Forms.Label labelTime4;
        private System.Windows.Forms.Label labelTime3;
        private System.Windows.Forms.Label labelTime2;
        private System.Windows.Forms.Label labelTime1;
    }
}
