﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.IO;

namespace Penteen
{
    partial class HelpBox : Form
    {
        public HelpBox()
        {
            InitializeComponent();
            this.Text = Application.ProductName;
            this.webBrowserHelp.Url = new Uri(
                Application.StartupPath + @"\Help\Index.htm");
        }
    }
}
